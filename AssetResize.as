/**
 * AssetResize is an AS3 class that will allow you to take an asset with an original width/height and resize it proportionally to the containers dimensions 
 * 
 * Developed by: James Wright
 * Date: 10/2012
 * License: MIT
 */
package display {
	
	import flash.display.Sprite;
	
	public class AssetResize extends Sprite {
		
		private static var containerHeight :Number;
		
		private static var containerWidth :Number;
		
		private static var latestStageHeight :Number;
		
		private static var latestStageWidth :Number;
		
		private static var loadedObjOriginalHeight :Number;
		
		private static var loadedObjOriginalWidth :Number;
		
		private static var loadedObjRatio :Number;
		
		private static var viewableAsset :*;
		
		
		public static function resizeAsset (asset :*, originalWidth :Number, originalHeight :Number, _containerWidth :Number, _containerHeight :Number) :void {
			
			viewableAsset = asset;
			
			containerWidth = _containerWidth;
			containerHeight = _containerHeight;
			
			loadedObjOriginalWidth = originalWidth;
			loadedObjOriginalHeight = originalHeight;
			
			loadedObjRatio = loadedObjOriginalWidth / loadedObjOriginalHeight;
			
			if (loadedObjOriginalWidth > loadedObjOriginalHeight) {
				resizeByVideoWidth(containerWidth);
			}
			else {
				
				resizeByVideoHeight(containerHeight);
			}
			alignAsset(asset);
			
		}
		
		public static function resizeByVideoHeight (height :Number) :void {
			
			viewableAsset.height = height;
			viewableAsset.width = height * loadedObjRatio;
			
			if (viewableAsset.width > containerWidth) {
				
				viewableAsset.width = containerWidth;
				viewableAsset.height = containerWidth / loadedObjRatio;
			}
			
		}
		
		public static function resizeByVideoWidth (width :Number) :void {
			
			viewableAsset.width = width;
			viewableAsset.height = width / loadedObjRatio;
			
			if (viewableAsset.height > containerHeight) {
				
				viewableAsset.height = containerHeight;
				viewableAsset.width = containerHeight * loadedObjRatio;
			}
			
		}
		
		private static function alignAsset (asset :*) :void {
			
			asset.x = (containerWidth / 2) - (asset.width / 2); //or //asset.y = (containerHeight / 2) - (asset.height / 2);
			
		}
		
		public function AssetResize () {
			
		}
	}
}
